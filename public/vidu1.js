ReactDOM.render(<h1>REact js</h1>,
    document.getElementById('root'));

class Person {
    constructor(name, age){
        this.age = age;
        this.name = name;
    }
    sayHello(){
        return 'xin chao, toi la' + this.name +  ', toi ' + this.age + ' tuoi';
    }
}

class Child extends Person{
    constructor(name, age, hobby){
        super(name,age);
        this.hobby = hobby;
    }
    sayHello(){
        return 'Xin chao em la' + this.name + ' , nam nay ' + this.age + ' thich choi' + this.hobby;
    }
}

var p2 = new Person('Hieu',2);
var aChild = new Child('Teo',3, 'maybay');
console.log(aChild.sayHello());
